package xm.crpt.recommendations;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import xm.crpt.recommendations.service.CryptoStatsCalculatorService;

@EnableBatchProcessing
@SpringBootApplication
public class RecommendationsApplication {

    public static void main(String[] args) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        ConfigurableApplicationContext ctx = SpringApplication.run(RecommendationsApplication.class, args);
        JobLauncher jobLauncher = (JobLauncher) ctx.getBean("jobLauncher");
        Job job1 = (Job) ctx.getBean("inputBTCCryptoPrice");
        Job job2 = (Job) ctx.getBean("inputDOGECryptoPrice");
        Job job3 = (Job) ctx.getBean("inputETHCryptoPrice");
        Job job4 = (Job) ctx.getBean("inputLTCCryptoPrice");
        Job job5 = (Job) ctx.getBean("inputXRPCryptoPrice");
        jobLauncher.run(job1, new JobParameters());
        jobLauncher.run(job2, new JobParameters());
        jobLauncher.run(job3, new JobParameters());
        jobLauncher.run(job4, new JobParameters());
        jobLauncher.run(job5, new JobParameters());
        ctx.getBean(CryptoStatsCalculatorService.class).calculateInitialCryptoStats();
    }

}
