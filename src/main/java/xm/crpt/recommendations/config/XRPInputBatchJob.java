package xm.crpt.recommendations.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import xm.crpt.recommendations.dto.InputCryptoPriceDto;
import xm.crpt.recommendations.dto.NormalizedInputCryptoPriceDto;

@Configuration
public class XRPInputBatchJob {


    @Bean
    public Job inputXRPCryptoPrice(JobBuilderFactory inputXRPJobBuilder,
                                    StepBuilderFactory inputXRPStepBuilder,
                                    ItemReader<InputCryptoPriceDto> inputXRPItemReader,
                                    ItemProcessor<InputCryptoPriceDto, NormalizedInputCryptoPriceDto> inputXRPItemProcessor,
                                    ItemWriter<NormalizedInputCryptoPriceDto> inputXRPItemWriter) {

        Step inputXRPStep = inputXRPStepBuilder
                .get("step")
                .<InputCryptoPriceDto, NormalizedInputCryptoPriceDto>chunk(5)
                .reader(inputXRPItemReader)
                .processor(inputXRPItemProcessor)
                .writer(inputXRPItemWriter)
                .build();

        return inputXRPJobBuilder.get("inputXRPCryptoPrice")
                .incrementer(new RunIdIncrementer())
                .start(inputXRPStep)
                .build();
    }

    @Bean
    public FlatFileItemReader<InputCryptoPriceDto> inputXRPItemReader(@Value("${xrpInput}") Resource resource) {

        FlatFileItemReader<InputCryptoPriceDto> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(resource);
        flatFileItemReader.setName("Covid stats CSV reader");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setLineMapper(inputXRPLineMapper());

        return flatFileItemReader;
    }

    @Bean
    public LineMapper<InputCryptoPriceDto> inputXRPLineMapper() {

        DefaultLineMapper<InputCryptoPriceDto> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("priceInstant", "type", "price");

        BeanWrapperFieldSetMapper<InputCryptoPriceDto> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(InputCryptoPriceDto.class);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper);

        return lineMapper;
    }
}
