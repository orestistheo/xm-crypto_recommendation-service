package xm.crpt.recommendations.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import xm.crpt.recommendations.dto.InputCryptoPriceDto;
import xm.crpt.recommendations.dto.NormalizedInputCryptoPriceDto;

@Configuration
public class DOGEInputBatchJob {

    @Bean
    public Job inputDOGECryptoPrice(JobBuilderFactory inputDOGEJobBuilder,
                                   StepBuilderFactory inputDOGEStepBuilder,
                                   ItemReader<InputCryptoPriceDto> inputDOGEItemReader,
                                   ItemProcessor<InputCryptoPriceDto, NormalizedInputCryptoPriceDto> inputDOGEItemProcessor,
                                   ItemWriter<NormalizedInputCryptoPriceDto> inputDOGEItemWriter) {

        Step inputDOGEStep = inputDOGEStepBuilder
                .get("step")
                .<InputCryptoPriceDto, NormalizedInputCryptoPriceDto>chunk(5)
                .reader(inputDOGEItemReader)
                .processor(inputDOGEItemProcessor)
                .writer(inputDOGEItemWriter)
                .build();

        return inputDOGEJobBuilder.get("inputDOGECryptoPrice")
                .incrementer(new RunIdIncrementer())
                .start(inputDOGEStep)
                .build();
    }

    @Bean
    public FlatFileItemReader<InputCryptoPriceDto> inputDOGEItemReader(@Value("${dogeInput}") Resource resource) {

        FlatFileItemReader<InputCryptoPriceDto> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(resource);
        flatFileItemReader.setName("Covid stats CSV reader");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setLineMapper(inputDOGELineMapper());

        return flatFileItemReader;
    }

    @Bean
    public LineMapper<InputCryptoPriceDto> inputDOGELineMapper() {

        DefaultLineMapper<InputCryptoPriceDto> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("priceInstant", "type", "price");

        BeanWrapperFieldSetMapper<InputCryptoPriceDto> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(InputCryptoPriceDto.class);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper);

        return lineMapper;
    }
}
