package xm.crpt.recommendations.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import xm.crpt.recommendations.dto.InputCryptoPriceDto;
import xm.crpt.recommendations.dto.NormalizedInputCryptoPriceDto;

@Configuration
public class ETHInputBatchJob {


    @Bean
    public Job inputETHCryptoPrice(JobBuilderFactory inputETHJobBuilder,
                                    StepBuilderFactory inputETHStepBuilder,
                                    ItemReader<InputCryptoPriceDto> inputETHItemReader,
                                    ItemProcessor<InputCryptoPriceDto, NormalizedInputCryptoPriceDto> inputETHItemProcessor,
                                    ItemWriter<NormalizedInputCryptoPriceDto> inputETHItemWriter) {

        Step inputETHStep = inputETHStepBuilder
                .get("step")
                .<InputCryptoPriceDto, NormalizedInputCryptoPriceDto>chunk(5)
                .reader(inputETHItemReader)
                .processor(inputETHItemProcessor)
                .writer(inputETHItemWriter)
                .build();

        return inputETHJobBuilder.get("inputETHCryptoPrice")
                .incrementer(new RunIdIncrementer())
                .start(inputETHStep)
                .build();
    }

    @Bean
    public FlatFileItemReader<InputCryptoPriceDto> inputETHItemReader(@Value("${ethInput}") Resource resource) {

        FlatFileItemReader<InputCryptoPriceDto> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(resource);
        flatFileItemReader.setName("Covid stats CSV reader");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setLineMapper(inputETHLineMapper());

        return flatFileItemReader;
    }

    @Bean
    public LineMapper<InputCryptoPriceDto> inputETHLineMapper() {

        DefaultLineMapper<InputCryptoPriceDto> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("priceInstant", "type", "price");

        BeanWrapperFieldSetMapper<InputCryptoPriceDto> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(InputCryptoPriceDto.class);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper);

        return lineMapper;
    }
}
