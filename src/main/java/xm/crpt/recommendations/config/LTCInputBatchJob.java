package xm.crpt.recommendations.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import xm.crpt.recommendations.dto.InputCryptoPriceDto;
import xm.crpt.recommendations.dto.NormalizedInputCryptoPriceDto;

@Configuration
public class LTCInputBatchJob {


    @Bean
    public Job inputLTCCryptoPrice(JobBuilderFactory inputLTCJobBuilder,
                                    StepBuilderFactory inputLTCStepBuilder,
                                    ItemReader<InputCryptoPriceDto> inputLTCItemReader,
                                    ItemProcessor<InputCryptoPriceDto, NormalizedInputCryptoPriceDto> inputLTCItemProcessor,
                                    ItemWriter<NormalizedInputCryptoPriceDto> inputLTCItemWriter) {

        Step inputLTCStep = inputLTCStepBuilder
                .get("step")
                .<InputCryptoPriceDto, NormalizedInputCryptoPriceDto>chunk(5)
                .reader(inputLTCItemReader)
                .processor(inputLTCItemProcessor)
                .writer(inputLTCItemWriter)
                .build();

        return inputLTCJobBuilder.get("inputLTCCryptoPrice")
                .incrementer(new RunIdIncrementer())
                .start(inputLTCStep)
                .build();
    }

    @Bean
    public FlatFileItemReader<InputCryptoPriceDto> inputLTCItemReader(@Value("${ltcInput}") Resource resource) {

        FlatFileItemReader<InputCryptoPriceDto> flatFileItemReader = new FlatFileItemReader<>();
        flatFileItemReader.setResource(resource);
        flatFileItemReader.setName("Covid stats CSV reader");
        flatFileItemReader.setLinesToSkip(1);
        flatFileItemReader.setLineMapper(inputLTCLineMapper());

        return flatFileItemReader;
    }

    @Bean
    public LineMapper<InputCryptoPriceDto> inputLTCLineMapper() {

        DefaultLineMapper<InputCryptoPriceDto> lineMapper = new DefaultLineMapper<>();
        DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();

        lineTokenizer.setDelimiter(",");
        lineTokenizer.setStrict(false);
        lineTokenizer.setNames("priceInstant", "type", "price");

        BeanWrapperFieldSetMapper<InputCryptoPriceDto> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
        fieldSetMapper.setTargetType(InputCryptoPriceDto.class);

        lineMapper.setLineTokenizer(lineTokenizer);
        lineMapper.setFieldSetMapper(fieldSetMapper);

        return lineMapper;
    }
}
