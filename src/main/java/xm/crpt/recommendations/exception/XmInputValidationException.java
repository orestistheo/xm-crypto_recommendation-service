package xm.crpt.recommendations.exception;

public class XmInputValidationException extends RuntimeException{

    private final String message;

    public XmInputValidationException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
