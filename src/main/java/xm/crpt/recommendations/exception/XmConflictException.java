package xm.crpt.recommendations.exception;

public class XmConflictException extends RuntimeException{

    private final String message;

    public XmConflictException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
