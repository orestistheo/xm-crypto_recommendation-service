package xm.crpt.recommendations.exception;

public class XmUnprocessableEntityException extends RuntimeException{

    private final String message;

    public XmUnprocessableEntityException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
