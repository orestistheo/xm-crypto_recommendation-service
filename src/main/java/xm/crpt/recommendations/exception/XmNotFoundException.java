package xm.crpt.recommendations.exception;

public class XmNotFoundException  extends RuntimeException{

    private final String message;

    public XmNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
