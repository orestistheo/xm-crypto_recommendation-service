package xm.crpt.recommendations.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import xm.crpt.recommendations.model.CryptoPrice;
import xm.crpt.recommendations.projection.CryptoEntityProjection;
import xm.crpt.recommendations.projection.CryptoPriceProjection;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface CryptoPriceRepository extends CrudRepository<CryptoPrice, Long> {

    @Query(value = "Select TOP 1 crp.ID, crp.PRICE_DATETIME as priceDatetime, crp.PRICE  from XM_CRYPTO_PRICE crp LEFT JOIN XM_CRYPTO_ENTITY cre ON crp.CRYPTO_ENTITY_ID=cre.ID where crp.PRICE_DATETIME>= ?1 and crp.PRICE_DATETIME<= ?2  and cre.id= ?3 ORDER BY crp.PRICE_DATETIME ASC ", nativeQuery = true)
    Optional<CryptoPriceProjection> findEarliestPriceBetweenDatesByCryptoEntity(Instant dateFrom, Instant dateTo, Long cryptoEntityId);

    @Query(value = "Select TOP 1 crp.ID, crp.PRICE_DATETIME as priceDatetime, crp.PRICE  from XM_CRYPTO_PRICE crp LEFT JOIN XM_CRYPTO_ENTITY cre ON crp.CRYPTO_ENTITY_ID=cre.ID where crp.PRICE_DATETIME>= ?1 and crp.PRICE_DATETIME<= ?2  and cre.id= ?3 ORDER BY crp.PRICE_DATETIME DESC ", nativeQuery = true)
    Optional<CryptoPriceProjection> findLatestPriceBetweenDatesByCryptoEntity(Instant dateFrom, Instant dateTo, Long cryptoEntityId);

    @Query(value = "Select crp.ID, crp.PRICE_DATETIME as priceDatetime, crp.PRICE  from XM_CRYPTO_PRICE crp LEFT JOIN XM_CRYPTO_ENTITY cre ON crp.CRYPTO_ENTITY_ID=cre.ID where crp.PRICE_DATETIME >= ?1 and crp.PRICE_DATETIME <= ?2 and cre.ID=?3  and crp.PRICE= (select MAX(innerCrp.PRICE) from XM_CRYPTO_PRICE innerCrp LEFT JOIN XM_CRYPTO_ENTITY innerCre ON innerCrp.CRYPTO_ENTITY_ID=innerCre.ID  WHERE innerCrp.PRICE_DATETIME>= ?1 and innerCrp.PRICE_DATETIME<= ?2  and innerCre.ID=?3)  ", nativeQuery = true)
    List<CryptoPriceProjection> findMaxPriceBetweenDatesByCryptoEntity(Instant dateFrom, Instant dateTo, Long cryptoEntityId);

    @Query(value = "Select crp.ID, crp.PRICE_DATETIME as priceDatetime, crp.PRICE  from XM_CRYPTO_PRICE crp LEFT JOIN XM_CRYPTO_ENTITY cre ON crp.CRYPTO_ENTITY_ID=cre.ID where crp.PRICE_DATETIME >= ?1 and crp.PRICE_DATETIME <= ?2 and cre.ID=?3 and crp.PRICE= (select MIN(innerCrp.PRICE) from XM_CRYPTO_PRICE innerCrp LEFT JOIN XM_CRYPTO_ENTITY innerCre ON innerCrp.CRYPTO_ENTITY_ID=innerCre.ID  WHERE innerCrp.PRICE_DATETIME>= ?1 and innerCrp.PRICE_DATETIME<= ?2  and innerCre.ID=?3)  ", nativeQuery = true)
    List<CryptoPriceProjection> findMinPriceBetweenDatesByCryptoEntity(Instant dateFrom, Instant dateTo, Long cryptoEntityId);

}
