package xm.crpt.recommendations.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import xm.crpt.recommendations.model.CryptoEntity;
import xm.crpt.recommendations.projection.CryptoEntityProjection;

import java.util.List;
import java.util.Optional;

public interface CryptoEntityRepository extends CrudRepository<CryptoEntity, Long> {

    Optional<CryptoEntity> findByType(String type);

    Page<CryptoEntity> findByArchived(final boolean archived, final Pageable pageable);

    @Query(value = "Select cre.ID, cre.TYPE, crs.NORMALIZED_RANGE as normalizedRange, cre.ARCHIVED, cre.ARCHIVED_DATE as archivedDate from XM_CRYPTO_ENTITY cre LEFT JOIN XM_CRYPTO_STATS crs ON cre.ID=crs.CRYPTO_ENTITY_ID where cre.ARCHIVED=false ORDER BY crs.NORMALIZED_RANGE DESC ", nativeQuery = true)
    List<CryptoEntityProjection> getDescendingListOfCryptos();
}
