package xm.crpt.recommendations.repository;

import org.springframework.data.repository.CrudRepository;
import xm.crpt.recommendations.model.CryptoStats;

import java.util.Optional;

public interface CryptoStatsRepository extends CrudRepository<CryptoStats, Long> {

    Optional<CryptoStats> findByCryptoEntityId(final Long cryptoEntityId);
}
