package xm.crpt.recommendations.batch;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;
import xm.crpt.recommendations.dto.InputCryptoPriceDto;
import xm.crpt.recommendations.dto.NormalizedInputCryptoPriceDto;

import java.time.Instant;

@Component
public class CryptoInputProcessor implements ItemProcessor<InputCryptoPriceDto, NormalizedInputCryptoPriceDto> {
    @Override
    public NormalizedInputCryptoPriceDto process(InputCryptoPriceDto inputCryptoPriceDto) {

        var instant = Instant.ofEpochMilli(Long.parseLong(inputCryptoPriceDto.getPriceInstant()));
        var price = Double.valueOf(inputCryptoPriceDto.getPrice());

        return new NormalizedInputCryptoPriceDto(instant, inputCryptoPriceDto.getType(), price);
    }
}
