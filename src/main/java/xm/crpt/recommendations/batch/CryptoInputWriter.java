package xm.crpt.recommendations.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;
import xm.crpt.recommendations.dto.NormalizedInputCryptoPriceDto;
import xm.crpt.recommendations.model.CryptoEntity;
import xm.crpt.recommendations.model.CryptoPrice;
import xm.crpt.recommendations.repository.CryptoEntityRepository;
import xm.crpt.recommendations.repository.CryptoPriceRepository;

import java.util.List;

@Component
public class CryptoInputWriter implements ItemWriter<NormalizedInputCryptoPriceDto> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CryptoInputWriter.class);

    private final CryptoEntityRepository cryptoEntityRepository;

    private final CryptoPriceRepository cryptoPriceRepository;

    public CryptoInputWriter(CryptoEntityRepository cryptoEntityRepository, CryptoPriceRepository cryptoPriceRepository) {
        this.cryptoEntityRepository = cryptoEntityRepository;
        this.cryptoPriceRepository = cryptoPriceRepository;
    }

    @Override
    public void write(List<? extends NormalizedInputCryptoPriceDto> normalizedInputCryptoPriceDtos) {
        for (var normalizedInputCryptoDto : normalizedInputCryptoPriceDtos) {
            CryptoEntity cryptoEntity;
            var optionalCryptoEntity = cryptoEntityRepository.findByType(normalizedInputCryptoDto.type());
            if (optionalCryptoEntity.isEmpty()) {
                cryptoEntity = new CryptoEntity();
                cryptoEntity.setType(normalizedInputCryptoPriceDtos.get(0).type());
                cryptoEntityRepository.save(cryptoEntity);
            } else {
                cryptoEntity = optionalCryptoEntity.get();
            }

            var cryptoPrice = new CryptoPrice();
            cryptoPrice.setPrice(normalizedInputCryptoDto.price());
            cryptoPrice.setPriceDatetime(normalizedInputCryptoDto.priceInstant());
            cryptoPrice.setCryptoEntity(cryptoEntity);
            cryptoPriceRepository.save(cryptoPrice);
        }
    }
}
