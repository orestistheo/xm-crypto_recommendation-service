package xm.crpt.recommendations.util;

public class Constants {

//    This value corresponds to Saturday, 1 January 2022 00:00:00 UTC
    public final static String DEFAULT_DATE_FROM_INSTANT = "1640995200000";

    //    This value corresponds to Monday, 31 January 2022 23:59:59 UTC
    public final static String DEFAULT_DATE_TO_INSTANT = "1643673599000";

    public final static String DATE_PATTERN = "yyyy-MM-dd";

    public final static int MAX_REQUESTS_PER_SECOND = 5;
}
