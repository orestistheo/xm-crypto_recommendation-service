package xm.crpt.recommendations.service;

import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import xm.crpt.recommendations.batch.CryptoInputProcessor;
import xm.crpt.recommendations.batch.CryptoInputWriter;
import xm.crpt.recommendations.dto.CreateCryptoEntityRequestDto;
import xm.crpt.recommendations.dto.CreateCryptoEntityResponseDto;
import xm.crpt.recommendations.dto.InputCryptoPriceDto;
import xm.crpt.recommendations.exception.XmConflictException;
import xm.crpt.recommendations.exception.XmNotFoundException;
import xm.crpt.recommendations.model.CryptoEntity;
import xm.crpt.recommendations.repository.CryptoEntityRepository;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CryptoEntityService {

    private final CryptoInputProcessor cryptoInputProcessor;

    private final CryptoInputWriter cryptoInputWriter;

    private final CryptoEntityRepository cryptoEntityRepository;

    private final CryptoStatsCalculatorService cryptoStatsCalculatorService;

    public CryptoEntityService(CryptoEntityRepository cryptoEntityRepository, CryptoInputProcessor cryptoInputProcessor, CryptoInputWriter cryptoInputWriter, CryptoStatsCalculatorService cryptoStatsCalculatorService) {
        this.cryptoEntityRepository = cryptoEntityRepository;
        this.cryptoInputProcessor = cryptoInputProcessor;
        this.cryptoInputWriter = cryptoInputWriter;
        this.cryptoStatsCalculatorService = cryptoStatsCalculatorService;
    }

    public void archive(Long cryptoEntityId) {
        var cryptoEntity = cryptoEntityRepository.findById(cryptoEntityId).orElseThrow(() -> new XmNotFoundException("Crypto entity now found"));

        if (cryptoEntity.isArchived()) {
            throw new XmConflictException("Crypto entity is already archived");
        }

        cryptoEntity.setArchived(true);
        cryptoEntity.setArchivedDate(Instant.now());
        cryptoEntityRepository.save(cryptoEntity);
    }

    public void undoArchive(Long cryptoEntityId) {
        var cryptoEntity = cryptoEntityRepository.findById(cryptoEntityId).orElseThrow(() -> new XmNotFoundException("Crypto entity now found"));

        if (!cryptoEntity.isArchived()) {
            throw new XmConflictException("Crypto entity is already unarchived");
        }

        cryptoEntity.setArchived(false);
        cryptoEntity.setArchivedDate(null);
        cryptoEntityRepository.save(cryptoEntity);
    }

    public void importCryptoStatsFromFile(MultipartFile multipartFile) throws IOException {
        List<InputCryptoPriceDto> cryptoPriceList = new ArrayList<>();
        InputStream inputStream = multipartFile.getInputStream();
        CsvParserSettings setting = new CsvParserSettings();
        setting.setHeaderExtractionEnabled(true);
        CsvParser parser = new CsvParser(setting);
        List<Record> cryptoPricesParsedList = parser.parseAllRecords(inputStream);
        cryptoPricesParsedList.forEach(record -> {
            InputCryptoPriceDto inputCryptoPriceDto = new InputCryptoPriceDto();
            inputCryptoPriceDto.setPrice(record.getString("price"));
            inputCryptoPriceDto.setPriceInstant(record.getString("timestamp"));
            inputCryptoPriceDto.setType(record.getString("symbol"));
            cryptoPriceList.add(inputCryptoPriceDto);
        });
        var normalizedCryptoPriceList = cryptoPriceList.stream().map(cryptoInputProcessor::process).collect(Collectors.toList());
        cryptoInputWriter.write(normalizedCryptoPriceList);
        cryptoStatsCalculatorService.calculateInitialCryptoStats();
    }


    public CreateCryptoEntityResponseDto createCryptoEntity(CreateCryptoEntityRequestDto createCryptoEntityRequestDto) {
        var cryptoEntity = new CryptoEntity();
        if (cryptoEntityRepository.findByType(createCryptoEntityRequestDto.type()).isPresent()) {
            throw new XmConflictException("A Crypto with the same type already exists");
        }
        cryptoEntity.setType(createCryptoEntityRequestDto.type());
        var savedCryptoEntity = cryptoEntityRepository.save(cryptoEntity);
        return new CreateCryptoEntityResponseDto(savedCryptoEntity.getId(), savedCryptoEntity.getType(), savedCryptoEntity.isArchived(), savedCryptoEntity.getArchivedDate());
    }
}
