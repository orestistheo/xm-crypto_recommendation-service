package xm.crpt.recommendations.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import xm.crpt.recommendations.batch.CryptoInputWriter;
import xm.crpt.recommendations.dto.CryptoEntityListResponseDto;
import xm.crpt.recommendations.dto.CryptoEntityResponseDto;
import xm.crpt.recommendations.dto.CryptoPriceListResponseDto;
import xm.crpt.recommendations.dto.CryptoPriceResponseDto;
import xm.crpt.recommendations.exception.XmConflictException;
import xm.crpt.recommendations.exception.XmInputValidationException;
import xm.crpt.recommendations.exception.XmUnprocessableEntityException;
import xm.crpt.recommendations.model.CryptoEntity;
import xm.crpt.recommendations.model.CryptoStats;
import xm.crpt.recommendations.repository.CryptoEntityRepository;
import xm.crpt.recommendations.repository.CryptoPriceRepository;
import xm.crpt.recommendations.repository.CryptoStatsRepository;
import xm.crpt.recommendations.util.Constants;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CryptoStatsCalculatorService {

    private static final Logger logger = LoggerFactory.getLogger(CryptoInputWriter.class);

    private final CryptoPriceRepository cryptoPriceRepository;

    private final CryptoEntityRepository cryptoEntityRepository;

    private final CryptoStatsRepository cryptoStatsRepository;

    public CryptoStatsCalculatorService(CryptoPriceRepository cryptoPriceRepository, CryptoEntityRepository cryptoEntityRepository, CryptoStatsRepository cryptoStatsRepository) {
        this.cryptoPriceRepository = cryptoPriceRepository;
        this.cryptoEntityRepository = cryptoEntityRepository;
        this.cryptoStatsRepository = cryptoStatsRepository;
    }

    public void calculateInitialCryptoStats() {
        var pageable = Pageable.ofSize(100);
        Page<CryptoEntity> cryptoEntityList = cryptoEntityRepository.findByArchived(false, pageable);
        do {
            for (var cryptoEntity : cryptoEntityList) {
                var normalizedRange = getNormalizedRangeByCryptoBetweenInstant(cryptoEntity.getId(), Long.parseLong(Constants.DEFAULT_DATE_FROM_INSTANT), Long.parseLong(Constants.DEFAULT_DATE_TO_INSTANT));
                CryptoStats cryptoStats;
                var optionalCryptoStats = cryptoStatsRepository.findByCryptoEntityId(cryptoEntity.getId());
                if (optionalCryptoStats.isEmpty()) {
                    cryptoStats = new CryptoStats();
                    cryptoStats.setNormalizedRange(normalizedRange);
                    cryptoStats.setCryptoEntity(cryptoEntity);
                } else {
                    cryptoStats = optionalCryptoStats.get();
                    cryptoStats.setNormalizedRange(normalizedRange);
                }
                cryptoStatsRepository.save(cryptoStats);
            }
            pageable = pageable.next();
            cryptoEntityList = cryptoEntityRepository.findByArchived(false, pageable);
        }
        while (!cryptoEntityList.isEmpty());
    }

    public CryptoPriceResponseDto findNewestPriceBetweenDatesByCryptoEntity(Long cryptoEntityId, Long dateFrom, Long dateTo) {
        var cryptoEntity = cryptoEntityRepository.findById(cryptoEntityId).orElseThrow(() -> new XmUnprocessableEntityException("Crypto Entity not found"));

        if (cryptoEntity.isArchived()) {
            throw new XmConflictException("Specified Crypto is archived");
        }
        var instantDateFrom = Instant.ofEpochMilli(dateFrom);
        var instantDateTo = Instant.ofEpochMilli(dateTo);
        if (instantDateFrom.isAfter(instantDateTo)) {
            throw new XmInputValidationException("Starting date limit is after the ending date limit");
        }
        var earliestPrice = cryptoPriceRepository.findEarliestPriceBetweenDatesByCryptoEntity(instantDateFrom, instantDateTo, cryptoEntity.getId());

        if (earliestPrice.isEmpty()) {
            throw new XmUnprocessableEntityException("There are no Prices for the specified Crypto in the specified date range");
        }
        return new CryptoPriceResponseDto(earliestPrice.get().getId(), earliestPrice.get().getPriceDatetime(), earliestPrice.get().getPrice(), cryptoEntity.getId(), cryptoEntity.getType());
    }

    public CryptoPriceResponseDto findLatestPriceBetweenDatesByCryptoEntity(Long cryptoEntityId, Long dateFrom, Long dateTo) {
        var cryptoEntity = cryptoEntityRepository.findById(cryptoEntityId).orElseThrow(() -> new XmUnprocessableEntityException("Crypto Entity not found"));
        if (cryptoEntity.isArchived()) {
            throw new XmConflictException("Specified Crypto is archived");
        }
        var instantDateFrom = Instant.ofEpochMilli(dateFrom);
        var instantDateTo = Instant.ofEpochMilli(dateTo);
        if (instantDateFrom.isAfter(instantDateTo)) {
            throw new XmInputValidationException("Starting date limit is after the ending date limit");
        }

        var latestPrice = cryptoPriceRepository.findLatestPriceBetweenDatesByCryptoEntity(instantDateFrom, instantDateTo, cryptoEntity.getId());

        if (latestPrice.isEmpty()) {
            throw new XmUnprocessableEntityException("There are no Prices for the specified Crypto in the specified date range");
        }
        return new CryptoPriceResponseDto(latestPrice.get().getId(), latestPrice.get().getPriceDatetime(), latestPrice.get().getPrice(), cryptoEntity.getId(), cryptoEntity.getType());
    }

    public CryptoPriceListResponseDto findMaxPriceBetweenDatesByCryptoEntity(Long cryptoEntityId, Long dateFrom, Long dateTo) {
        var cryptoEntity = cryptoEntityRepository.findById(cryptoEntityId).orElseThrow(() -> new XmUnprocessableEntityException("Crypto Entity not found"));
        if (cryptoEntity.isArchived()) {
            throw new XmConflictException("Specified Crypto is archived");
        }
        var instantDateFrom = Instant.ofEpochMilli(dateFrom);
        var instantDateTo = Instant.ofEpochMilli(dateTo);
        if (instantDateFrom.isAfter(instantDateTo)) {
            throw new XmInputValidationException("Starting date limit is after the ending date limit");
        }

        var maxCryptoPriceList = cryptoPriceRepository.findMaxPriceBetweenDatesByCryptoEntity(instantDateFrom, instantDateTo, cryptoEntity.getId());

        var cryptoResponseDtoList = maxCryptoPriceList.stream().map(cryptoPriceProjection -> new CryptoPriceResponseDto(cryptoPriceProjection.getId(), cryptoPriceProjection.getPriceDatetime(), cryptoPriceProjection.getPrice(), cryptoEntity.getId(), cryptoEntity.getType())).collect(Collectors.toList());
        return new CryptoPriceListResponseDto(cryptoResponseDtoList);
    }

    public CryptoPriceListResponseDto findMinPriceBetweenDatesByCryptoEntity(Long cryptoEntityId, Long dateFrom, Long dateTo) {
        var cryptoEntity = cryptoEntityRepository.findById(cryptoEntityId).orElseThrow(() -> new XmUnprocessableEntityException("Crypto Entity not found"));
        if (cryptoEntity.isArchived()) {
            throw new XmConflictException("Specified Crypto is archived");
        }
        var instantDateFrom = Instant.ofEpochMilli(dateFrom);
        var instantDateTo = Instant.ofEpochMilli(dateTo);

        if (instantDateFrom.isAfter(instantDateTo)) {
            throw new XmInputValidationException("Starting date limit is after the ending date limit");
        }

        var minCryptoPriceList = cryptoPriceRepository.findMinPriceBetweenDatesByCryptoEntity(instantDateFrom, instantDateTo, cryptoEntity.getId());

        var cryptoResponseDtoList = minCryptoPriceList.stream().map(cryptoPriceProjection -> new CryptoPriceResponseDto(cryptoPriceProjection.getId(), cryptoPriceProjection.getPriceDatetime(), cryptoPriceProjection.getPrice(), cryptoEntity.getId(), cryptoEntity.getType())).collect(Collectors.toList());
        return new CryptoPriceListResponseDto(cryptoResponseDtoList);
    }


    public CryptoEntityListResponseDto getDescendingListOfCryptos() {
        var descendingCryptoEntities = cryptoEntityRepository.getDescendingListOfCryptos();
        var cryptoEntity = descendingCryptoEntities.stream().map(cryptoEntityProjection -> new CryptoEntityResponseDto(cryptoEntityProjection.getId(), cryptoEntityProjection.getType(), cryptoEntityProjection.getArchived(), cryptoEntityProjection.getArchivedDate(), cryptoEntityProjection.getNormalizedRange())).collect(Collectors.toList());
        return new CryptoEntityListResponseDto(cryptoEntity);
    }


    public CryptoEntityListResponseDto getCryptoWithHighestNormalizedRangeByDay(LocalDate date) {

        if (date.isAfter(LocalDate.now())) {
            throw new XmInputValidationException("Input date is in the future");
        }
        var instantFrom = date.atStartOfDay().atZone(ZoneId.of("UTC")).toInstant().toEpochMilli();
        var instantTo = date.atStartOfDay().plusDays(1L).atZone(ZoneId.of("UTC")).toInstant().toEpochMilli();
        Set<CryptoEntityResponseDto> highestCryptoEntitySet = new HashSet<>();

        var pageable = Pageable.ofSize(100);
        Page<CryptoEntity> cryptoEntityList = cryptoEntityRepository.findByArchived(false, pageable);
        do {
            for (var cryptoEntity : cryptoEntityList) {
                var normalizedRange = getNormalizedRangeByCryptoBetweenInstant(cryptoEntity.getId(), instantFrom, instantTo);

                if (highestCryptoEntitySet.isEmpty()) {
                    highestCryptoEntitySet.add(new CryptoEntityResponseDto(cryptoEntity.getId(), cryptoEntity.getType(), cryptoEntity.isArchived(), cryptoEntity.getArchivedDate(), normalizedRange));
                } else if (highestCryptoEntitySet.stream().anyMatch(highestCryptoEntity -> normalizedRange > highestCryptoEntity.normalizedRange())) {
                    highestCryptoEntitySet.clear();
                    highestCryptoEntitySet.add(new CryptoEntityResponseDto(cryptoEntity.getId(), cryptoEntity.getType(), cryptoEntity.isArchived(), cryptoEntity.getArchivedDate(), normalizedRange));
                } else if (highestCryptoEntitySet.stream().anyMatch(highestCryptoEntity -> normalizedRange.equals(highestCryptoEntity.normalizedRange()))) {
                    highestCryptoEntitySet.add(new CryptoEntityResponseDto(cryptoEntity.getId(), cryptoEntity.getType(), cryptoEntity.isArchived(), cryptoEntity.getArchivedDate(), normalizedRange));
                }
            }
            pageable = pageable.next();
            cryptoEntityList = cryptoEntityRepository.findByArchived(false, pageable);
        }
        while (!cryptoEntityList.isEmpty());

        return new CryptoEntityListResponseDto(new ArrayList<>(highestCryptoEntitySet));

    }

    public Double getNormalizedRangeByCryptoBetweenInstant(Long cryptoId, Long instantFrom, Long instantTo) {
        var maxPriceDto = findMaxPriceBetweenDatesByCryptoEntity(cryptoId, instantFrom, instantTo);
        var minPriceDto = findMinPriceBetweenDatesByCryptoEntity(cryptoId, instantFrom, instantTo);

        if (minPriceDto.cryptoPriceResponseDtoList().isEmpty() || maxPriceDto.cryptoPriceResponseDtoList().isEmpty()) {
            return 0d;
        }
        var max = maxPriceDto.cryptoPriceResponseDtoList().get(0).price();
        var min = minPriceDto.cryptoPriceResponseDtoList().get(0).price();
        return ((max - min) / min);
    }
}
