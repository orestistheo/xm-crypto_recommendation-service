package xm.crpt.recommendations.service;

import org.springframework.stereotype.Service;
import xm.crpt.recommendations.repository.CryptoEntityRepository;
import xm.crpt.recommendations.repository.CryptoPriceRepository;

@Service
public class CryptoPriceService {

    private final CryptoPriceRepository cryptoPriceRepository;

    private final CryptoEntityRepository cryptoEntityRepository;

    public CryptoPriceService(CryptoPriceRepository cryptoPriceRepository, CryptoEntityRepository cryptoEntityRepository) {
        this.cryptoPriceRepository = cryptoPriceRepository;
        this.cryptoEntityRepository = cryptoEntityRepository;
    }


}
