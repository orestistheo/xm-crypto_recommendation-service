package xm.crpt.recommendations.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

@Entity
@Table(name = "XM_CRYPTO_PRICE", uniqueConstraints=
@UniqueConstraint(columnNames={"PRICE_DATETIME", "CRYPTO_ENTITY_ID"}))
public class CryptoPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "PRICE_DATETIME", nullable = false)
    private Instant priceDatetime;

    @Column(name = "PRICE", nullable = false)
    private Double price;

    @ManyToOne
    @JoinColumn(name = "CRYPTO_ENTITY_ID")
    private CryptoEntity cryptoEntity;

    public CryptoPrice() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getPriceDatetime() {
        return priceDatetime;
    }

    public void setPriceDatetime(Instant priceDatetime) {
        this.priceDatetime = priceDatetime;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public CryptoEntity getCryptoEntity() {
        return cryptoEntity;
    }

    public void setCryptoEntity(CryptoEntity cryptoEntity) {
        this.cryptoEntity = cryptoEntity;
    }
}
