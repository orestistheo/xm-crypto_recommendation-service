package xm.crpt.recommendations.model;

import javax.persistence.*;

@Entity
@Table(name = "XM_CRYPTO_STATS")
public class CryptoStats {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CRYPTO_ENTITY_ID", nullable = false, updatable = false)
    private CryptoEntity cryptoEntity;

    @Column(name = "NORMALIZED_RANGE")
    private Double normalizedRange;


    public CryptoStats() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CryptoEntity getCryptoEntity() {
        return cryptoEntity;
    }

    public void setCryptoEntity(CryptoEntity cryptoEntity) {
        this.cryptoEntity = cryptoEntity;
    }

    public Double getNormalizedRange() {
        return normalizedRange;
    }

    public void setNormalizedRange(Double normalizedRange) {
        this.normalizedRange = normalizedRange;
    }
}
