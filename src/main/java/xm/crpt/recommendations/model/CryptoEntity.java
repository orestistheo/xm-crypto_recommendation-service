package xm.crpt.recommendations.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "XM_CRYPTO_ENTITY")
public class CryptoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "TYPE", nullable = false, updatable = false, unique = true)
    private String type;

    @Column(name = "ARCHIVED")
    private boolean archived;

    @Column(name = "ARCHIVED_DATE")
    private Instant archivedDate;

    @OneToMany(targetEntity = CryptoPrice.class, cascade = CascadeType.ALL, mappedBy = "cryptoEntity")
    private List<CryptoPrice> cryptoPrices;

    @OneToOne(mappedBy = "cryptoEntity", cascade = CascadeType.ALL,
            fetch = FetchType.LAZY)
    private CryptoStats cryptoStats;

    public CryptoEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Instant getArchivedDate() {
        return archivedDate;
    }

    public void setArchivedDate(Instant archivedDate) {
        this.archivedDate = archivedDate;
    }

    public List<CryptoPrice> getCryptoPrices() {
        return cryptoPrices;
    }

    public void setCryptoPrices(List<CryptoPrice> cryptoPrices) {
        this.cryptoPrices = cryptoPrices;
    }

    public CryptoStats getCryptoStats() {
        return cryptoStats;
    }

    public void setCryptoStats(CryptoStats cryptoStats) {
        this.cryptoStats = cryptoStats;
    }
}
