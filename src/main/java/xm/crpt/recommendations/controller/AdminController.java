package xm.crpt.recommendations.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xm.crpt.recommendations.service.CryptoEntityService;

@RestController
@RequestMapping("admin")
public class AdminController {

    private final CryptoEntityService cryptoEntityService;

    public AdminController(CryptoEntityService cryptoEntityService) {
        this.cryptoEntityService = cryptoEntityService;
    }

    @PatchMapping("/entity/{id}/archive/")
    @ResponseStatus(value = HttpStatus.OK)
    public void archive(@PathVariable Long id) {
        cryptoEntityService.archive(id);
    }

    @PatchMapping("/entity/{id}/undo-archive/")
    @ResponseStatus(value = HttpStatus.OK)
    public void undoArchive(@PathVariable Long id) {
        cryptoEntityService.undoArchive(id);
    }


    @Operation(description = "Reads a CSV file containing prices about Crypto entities. Creates the Crypto entity if it does not exist and imports the price values.")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/upload-crypto-price-data")
    public void uploadCryptoPriceData(@Parameter(description = "A multipart csv file. The first line contains the headers timestamp,symbol,price and the following lines the data corresponding to the headers") @RequestParam("file") MultipartFile file) throws Exception {
        cryptoEntityService.importCryptoStatsFromFile(file);
    }
}
