package xm.crpt.recommendations.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import xm.crpt.recommendations.dto.CryptoEntityListResponseDto;
import xm.crpt.recommendations.dto.CryptoPriceListResponseDto;
import xm.crpt.recommendations.dto.CryptoPriceResponseDto;
import xm.crpt.recommendations.service.CryptoPriceService;
import xm.crpt.recommendations.service.CryptoStatsCalculatorService;
import xm.crpt.recommendations.util.Constants;

import java.time.LocalDate;

@RestController
@RequestMapping("crypto")
public class RecommendationController {

    private final CryptoPriceService cryptoPriceService;

    private final CryptoStatsCalculatorService cryptoStatsCalculatorService;

    public RecommendationController(CryptoPriceService cryptoPriceService, CryptoStatsCalculatorService cryptoStatsCalculatorService) {
        this.cryptoPriceService = cryptoPriceService;
        this.cryptoStatsCalculatorService = cryptoStatsCalculatorService;
    }

    @Operation(summary = "Retrieve a list of Cryptos sorted by normalized range for the whole month")
    @GetMapping("/by-normalized-range/descending")
    public CryptoEntityListResponseDto getDescendingListOfCryptos() {

        return cryptoStatsCalculatorService.getDescendingListOfCryptos();
    }

    @Operation(summary = "Retrieve the latest Crypto price for a specific Crypto entity in a specified date range")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Latest price for Crypto entity found", content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "404", description = "Invalid input parameters", content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "409", description = "Specified Crypto is archived", content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "422", description = "Unable to retrieve Crypto price for the specified input parameters", content = @Content(mediaType = "application/json")),
            @ApiResponse(responseCode = "500", description = "Unknown Error. Please contact the manufacturer", content = @Content(mediaType = "application/json"))
    })
    @GetMapping(value = "/latest-price/{id}/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CryptoPriceResponseDto getLatestCryptoPriceBetweenDatesByCrypto(@Parameter(description = "Id of Crypto entity to get stats for") @PathVariable("id") Long id, @Parameter(description = "Lower date limit to search for Crypto stats. The date is input as Unix epoch time number.") @RequestParam(required = false, defaultValue = Constants.DEFAULT_DATE_FROM_INSTANT) Long dateFrom, @Parameter(description = "Upper date limit to search for Crypto stats.  The date is input as Unix epoch time number.") @RequestParam(required = false, defaultValue = Constants.DEFAULT_DATE_TO_INSTANT) Long dateTo) {

        return cryptoStatsCalculatorService.findLatestPriceBetweenDatesByCryptoEntity(id, dateFrom, dateTo);
    }

    @Operation(summary = "Retrieve the newest Crypto price for a specific Crypto entity in a specified date range")
    @GetMapping(value = "/newest-price/{id}/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CryptoPriceResponseDto getNewestCryptoPriceBetweenDatesByCrypto(@Parameter(description = "Id of Crypto entity to get stats for") @PathVariable("id") Long id, @Parameter(description = "Lower date limit to search for Crypto stats") @RequestParam(required = false, defaultValue = Constants.DEFAULT_DATE_FROM_INSTANT) Long dateFrom, @Parameter(description = "Upper date limit to search for Crypto stats") @RequestParam(required = false, defaultValue = Constants.DEFAULT_DATE_TO_INSTANT) Long dateTo) {

        return cryptoStatsCalculatorService.findNewestPriceBetweenDatesByCryptoEntity(id, dateFrom, dateTo);
    }

    @Operation(summary = "Retrieve the max Crypto price for a specific Crypto entity in a specified date range")
    @GetMapping(value = "/max-price/{id}/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CryptoPriceListResponseDto getMaxPriceBetweenDatesByCrypto(@Parameter(description = "Id of Crypto entity to get stats for") @PathVariable("id") Long id, @Parameter(description = "Lower date limit to search for Crypto stats") @RequestParam(required = false, defaultValue = Constants.DEFAULT_DATE_FROM_INSTANT) Long dateFrom, @Parameter(description = "Upper date limit to search for Crypto stats") @RequestParam(required = false, defaultValue = Constants.DEFAULT_DATE_TO_INSTANT) Long dateTo) {

        return cryptoStatsCalculatorService.findMaxPriceBetweenDatesByCryptoEntity(id, dateFrom, dateTo);
    }

    @Operation(summary = "Retrieve the lowest Crypto price for a specific Crypto entity in a specified date range")
    @GetMapping(value = "/min-price/{id}/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CryptoPriceListResponseDto getMinPriceBetweenDatesByCrypto(@Parameter(description = "Id of Crypto entity to get stats for") @PathVariable("id") Long id, @Parameter(description = "Lower date limit to search for Crypto stats") @RequestParam(required = false, defaultValue = Constants.DEFAULT_DATE_FROM_INSTANT) Long dateFrom, @Parameter(description = "Upper date limit to search for Crypto stats") @RequestParam(required = false, defaultValue = Constants.DEFAULT_DATE_TO_INSTANT) Long dateTo) {

        return cryptoStatsCalculatorService.findMinPriceBetweenDatesByCryptoEntity(id, dateFrom, dateTo);
    }

    @Operation(summary = "Retrieve the Crypto with the highest normalized range for a specific day")
    @GetMapping(value = "/highest-normalized-range/{date}/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CryptoEntityListResponseDto getCryptoWithHighestNormalizedRangeByDate(@Parameter(description = "Date to search for Crypto. Format is yyyy-MM-dd") @PathVariable("date") @DateTimeFormat(pattern = Constants.DATE_PATTERN) LocalDate date) {

        return cryptoStatsCalculatorService.getCryptoWithHighestNormalizedRangeByDay(date);
    }
}
