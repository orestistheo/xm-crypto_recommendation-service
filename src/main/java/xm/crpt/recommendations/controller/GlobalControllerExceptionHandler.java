package xm.crpt.recommendations.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import xm.crpt.recommendations.dto.ApiExceptionResponseDto;
import xm.crpt.recommendations.exception.XmConflictException;
import xm.crpt.recommendations.exception.XmInputValidationException;
import xm.crpt.recommendations.exception.XmNotFoundException;
import xm.crpt.recommendations.exception.XmUnprocessableEntityException;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);


    @ExceptionHandler(XmNotFoundException.class)
    public ResponseEntity<ApiExceptionResponseDto> handleNotFoundException(XmNotFoundException xmNotFoundException) {
        var apiExceptionResponse = new ApiExceptionResponseDto(xmNotFoundException.getMessage(), HttpStatus.NOT_FOUND);
        logException(apiExceptionResponse);
        return new ResponseEntity<>(apiExceptionResponse, apiExceptionResponse.statusCode());
    }

    @ExceptionHandler(XmConflictException.class)
    public ResponseEntity<ApiExceptionResponseDto> handleConflictException(XmConflictException xmConflictException) {
        var apiExceptionResponse = new ApiExceptionResponseDto(xmConflictException.getMessage(), HttpStatus.CONFLICT);
        logException(apiExceptionResponse);
        return new ResponseEntity<>(apiExceptionResponse, apiExceptionResponse.statusCode());
    }

    @ExceptionHandler(XmInputValidationException.class)
    public ResponseEntity<ApiExceptionResponseDto> handleInputValidationException(XmInputValidationException xmInputValidationException) {
        var apiExceptionResponse = new ApiExceptionResponseDto(xmInputValidationException.getMessage(), HttpStatus.BAD_REQUEST);
        logException(apiExceptionResponse);
        return new ResponseEntity<>(apiExceptionResponse, apiExceptionResponse.statusCode());
    }

    @ExceptionHandler(XmUnprocessableEntityException.class)
    public ResponseEntity<ApiExceptionResponseDto> handleUnprocessableEntityException(XmUnprocessableEntityException xmUnprocessableEntityException) {
        var apiExceptionResponse = new ApiExceptionResponseDto(xmUnprocessableEntityException.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
        logException(apiExceptionResponse);
        return new ResponseEntity<>(apiExceptionResponse, apiExceptionResponse.statusCode());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiExceptionResponseDto> handleUnknownException(Exception exception) {
        var apiExceptionResponse = new ApiExceptionResponseDto(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        logException(apiExceptionResponse);
        return new ResponseEntity<>(apiExceptionResponse, apiExceptionResponse.statusCode());
    }

    private void logException(ApiExceptionResponseDto apiExceptionResponseDto) {
        logger.debug(apiExceptionResponseDto.message());
    }
}
