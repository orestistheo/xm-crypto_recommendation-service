package xm.crpt.recommendations.dto;

import java.util.List;

public record CryptoEntityListResponseDto(List<CryptoEntityResponseDto> cryptoEntityResponseDtoList) {
}
