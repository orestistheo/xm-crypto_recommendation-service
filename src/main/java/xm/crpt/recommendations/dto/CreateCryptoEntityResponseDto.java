package xm.crpt.recommendations.dto;

import java.time.Instant;

public record CreateCryptoEntityResponseDto(Long id, String type, boolean archived, Instant archivedDate) {
}
