package xm.crpt.recommendations.dto;


public class InputCryptoPriceDto {

    private String priceInstant;

    private String type;

    private String price;

    public InputCryptoPriceDto() {
    }

    public String getPriceInstant() {
        return priceInstant;
    }

    public String getType() {
        return type;
    }

    public String getPrice() {
        return price;
    }

    public void setPriceInstant(String priceInstant) {
        this.priceInstant = priceInstant;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
