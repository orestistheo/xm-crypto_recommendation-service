package xm.crpt.recommendations.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.Instant;

public record CryptoPriceResponseDto(Long id, @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "UTC") Instant priceDatetime, Double price, Long cryptoEntityId,
                                     String cryptoEntityType) {


}
