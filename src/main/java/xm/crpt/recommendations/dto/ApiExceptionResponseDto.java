package xm.crpt.recommendations.dto;

import org.springframework.http.HttpStatus;

public record ApiExceptionResponseDto(String message, HttpStatus statusCode) {
}
