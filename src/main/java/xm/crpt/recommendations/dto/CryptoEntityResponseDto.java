package xm.crpt.recommendations.dto;

import java.time.Instant;

public record CryptoEntityResponseDto(Long id, String type, boolean archived, Instant archivedDate, Double normalizedRange) {
}
