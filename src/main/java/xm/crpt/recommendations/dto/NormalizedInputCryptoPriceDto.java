package xm.crpt.recommendations.dto;

import java.time.Instant;

public record NormalizedInputCryptoPriceDto(Instant priceInstant, String type, Double price) {
}
