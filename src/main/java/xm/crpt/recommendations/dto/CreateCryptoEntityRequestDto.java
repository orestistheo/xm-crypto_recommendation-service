package xm.crpt.recommendations.dto;

import javax.validation.constraints.NotNull;

public record CreateCryptoEntityRequestDto(@NotNull String type) {
}
