package xm.crpt.recommendations.dto;

import java.util.List;

public record CryptoPriceListResponseDto (List<CryptoPriceResponseDto> cryptoPriceResponseDtoList){
}
