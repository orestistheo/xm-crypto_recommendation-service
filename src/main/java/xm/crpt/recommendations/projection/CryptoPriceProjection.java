package xm.crpt.recommendations.projection;

import java.time.Instant;

public interface CryptoPriceProjection {

    Long getId();

    Instant getPriceDatetime();

    Double getPrice();
}
