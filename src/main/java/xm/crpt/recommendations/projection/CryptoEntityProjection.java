package xm.crpt.recommendations.projection;

import java.time.Instant;

public interface CryptoEntityProjection {
    Long getId();

    String getType();

    boolean getArchived();

    Instant getArchivedDate();

    Double getNormalizedRange();
}
