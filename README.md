## Documentation for endpoints

Swagger UI Endpoint: http://localhost:8080/swagger-ui/index.html

Swagger Docs Endpoint http://localhost:8080/v3/api-docs/

## Running crypto recommendation service in a container using Docker

* You need to have Docker and Apache maven installed on your PC 

* Run mvn clean install command to generate the jar file in the target folder

* Navigate to the project folder that contains the Dockerfile file and open a terminal

* Run the following command to create the Docker image of recommendation service

       docker build -t xm-rm:latest -f Dockerfile .

* Run the following command to run the server in a container in port 8081 in detach mode

       docker run -p 8081:8080 xm-rm -d

## Example endpoints

Retrieve the min value of a Crypto between the specified timestamps

GET http://localhost:8080/crypto/min-price/1/?dateFrom=1642093200000&dateTo=1642896000000

## Importing a new Crypto in the system

POST http://localhost:8080/admin/upload-crypto-price-data

Use the file in the exampleFile folder in the body of the request as multipart file with key=file and value the
aforementioned file.


